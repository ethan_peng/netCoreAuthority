﻿using Microsoft.AspNetCore.Mvc;

namespace Auth.Web.Controllers
{
    public class SharedController : Controller
    {
        // GET: /<controller>/
        public IActionResult Error()
        {
            return View();
        }
    }
}
