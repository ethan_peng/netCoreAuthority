﻿using Auth.Application;
using Auth.Application.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace Auth.Web.Controllers
{
    public class HomeController : AuthControllerBase
    {
        private readonly IHttpContextAccessor _httpContext;
        private readonly IUserAppService _userService;
        public HomeController(IHttpContextAccessor httpContext, IUserAppService userService)
        {
            _httpContext = httpContext;
            _userService = userService;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
           var userIs= _httpContext.HttpContext.Session.GetString("CurrentUserId");
            UserDto user =_userService.QuerySingle(userIs);
            ViewData["User"] = user;
            return View();
        }

        public IActionResult GetIcon()
        {
            ViewData["Title"] = "选择图标";
            return View();
        }
    }
}

