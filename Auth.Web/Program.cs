﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Auth.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls("http://*:5123")
                .UseKestrel()
                .UseStartup<Startup>();


        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
              .UseStartup<Startup>()
              .Build();
    }
}