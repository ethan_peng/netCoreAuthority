﻿using Auth.Application;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.Application.Dtos;

namespace Auth.Web.Components
{
    [ViewComponent(Name = "Navigation")]
    public class NavigationViewComponent : ViewComponent
    {
        private readonly IMenuAppService _menuAppService;
        private readonly IUserAppService _userAppService;
        public NavigationViewComponent(IMenuAppService menuAppService, IUserAppService userAppService)
        {
            _menuAppService = menuAppService;
            _userAppService = userAppService;
        }

        public IViewComponentResult Invoke()
        {
            var userId = HttpContext.Session.GetString("CurrentUserId");
            var  menus = _menuAppService.GetMenusByUser(userId);
            ViewData["MenuHtml"] = GetMenu(menus);
            return View();
        }
        /// <summary>
        /// 获取菜单
        /// </summary>
        /// <param name="menuDtos"></param>
        /// <returns></returns>
        private string GetMenu(List<MenuDto> menuDtos)
        {
            StringBuilder sb = new StringBuilder();
            var parentMenu = menuDtos.Where(s => s.No.Length < 4).ToList();
            foreach (var item in parentMenu)
            {
                var childMenu = menuDtos.Where(s => s.ParentId == item.Id).ToList();
                if (childMenu.Any())
                {
                    sb.Append(
                        $"<li ><a href = \"javascript:;\" ><i class=\"layui-icon\">{item.Icon}</i><cite>{item.Name}" +
                        $"</cite><i class=\"iconfont nav_right\">&#xe697;</i></a>");
                    GetChildMenu(menuDtos, item.Id,sb);
                    sb.Append("</li>");
                }
                else
                {
                    sb.Append($"<li><a _href = \"{item.Url}\" ><i class=\"layui-icon\">{item.Icon}</i><cite>{item.Name}" +
                              $"</cite></a> </li >");
                }
            }

            return sb.ToString();
        }
        /// <summary>
        /// 遍历子菜单
        /// </summary>
        /// <param name="menuDtos"></param>
        /// <param name="id"></param>
        /// <param name="sb"></param>
        public void GetChildMenu(List<MenuDto> menuDtos, string id, StringBuilder sb)
        {
            sb.Append("<ul class=\"sub-menu\">");
            var menuList = menuDtos.Where(s => s.ParentId == id);
            foreach (var item in menuList)
            {
                var childMenu = menuDtos.Where(s => s.ParentId == item.Id);
                if (childMenu.Any())
                {
                    sb.Append($"<li><a href = \"javascript:;\" ><i class=\"layui-icon\">{item.Icon}</i><cite>{item.Name}" +
                        $"</cite><i class=\"iconfont nav_right\">&#xe697;</i></a>");
                    GetChildMenu(menuDtos, item.Id, sb);
                    sb.Append("</li>");
                }
                else
                {
                    sb.Append($"<li><a _href = \"{item.Url}\" ><i class=\"layui-icon\">{item.Icon}</i><cite>{item.Name}" +
                              $"</cite></a> </li >");
                }
            }
            sb.Append("</ul>");
        }
    }
}