﻿using System.Text;
using Auth.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Logging;

namespace Auth.Web.Filter
{
    public class CustomerExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IModelMetadataProvider _modelMetadataProvider;
        private readonly ILogger<CustomerExceptionFilterAttribute> _logger;

        /// <summary>
        /// 错误页面地址
        /// </summary>
        public static string ErrorPageUrl = "~/Views/Shared/Error.cshtml";

        public CustomerExceptionFilterAttribute(
            IHostingEnvironment hostingEnvironment,
            IModelMetadataProvider modelMetadataProvider,
            ILogger<CustomerExceptionFilterAttribute> logger
        )
        {
            _hostingEnvironment = hostingEnvironment;
            _modelMetadataProvider = modelMetadataProvider;
            _logger = logger;
        }

        /// <summary>
        /// 异常拦截
        /// </summary>
        /// <param name="context"></param>
        public override void OnException(ExceptionContext context)
        {
            //判断是否为 ajax 请求
            var IsAjaxRequest = context.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";

            //判断是否是自定义异常类型
            if (context.Exception is MessageBox)
            {

                if (IsAjaxRequest)
                {
                    context.Result = new JsonResult(MessageBox.errorModel);
                }
                else
                {
                    var errorModel = new ErrorModel(context.Exception.Message);
                    var sb = new StringBuilder();
                    sb.Append("<script src=\"/lib/jquery/jquery-2.1.4.min.js\"></script>");
                    sb.Append("<script src=\"/lib/layui/layui.js\"></script>");
                    sb.Append("<script type='text/javascript'>");
                    sb.Append("$(function(){  layui.use('layer',function() { top.layer.msg('" +
                              errorModel.msg.Trim().Replace("'", "“").Replace("\"", "”") + "',{icon:2,offset: 't',anim: 6}); });});");
                    sb.Append("</script>");
                    context.Result =
                        new ContentResult() { Content = sb.ToString(), ContentType = "text/html;charset=utf-8;" };
                }
                context.HttpContext.Response.StatusCode = 200;
            }
            else
            {
                var errorModel = new ErrorModel(context.Exception);
                if (IsAjaxRequest)
                {
                    context.Result = new JsonResult(errorModel);
                }
                else
                {
                    var result = new ViewResult() { ViewName = ErrorPageUrl };
                    result.ViewData = new ViewDataDictionary(_modelMetadataProvider, context.ModelState);
                    result.ViewData.Add("Exception", errorModel);
                    context.Result = result;
                }
            }
            _logger.LogError(context.Exception, context.HttpContext.Connection.RemoteIpAddress.ToString());
            context.HttpContext.Response.StatusCode = 200;
            //表示异常已处理
            context.ExceptionHandled = true;
        }

        //public override Task OnExceptionAsync(ExceptionContext context)
        //{
        //    return base.OnExceptionAsync(context);
        //}
    }
}