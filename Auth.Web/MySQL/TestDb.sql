/*
 Navicat Premium Data Transfer

 Source Server         : 本地MySQL
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : testdb

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 10/09/2018 15:30:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for departments
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ContactNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CreateTime` datetime(0) NULL DEFAULT NULL,
  `CreateUserId` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IsDeleted` int(11) NOT NULL,
  `Manager` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ParentId` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of departments
-- ----------------------------
INSERT INTO `departments` VALUES ('142415ba-c154-4f17-89e5-578b812c0cb4', '101', 'ceshi', NULL, '2018-09-10 09:52:15', '00000000-0000-0000-0000-000000000000', 0, NULL, '测试部门', '00000000-0000-0000-0000-000000000000', '11');
INSERT INTO `departments` VALUES ('4a3482e8-5205-4204-839e-4601e112e3a1', '10001', '11', '1', NULL, '4a3482e8-5205-4204-839e-4601e112e3aa', 0, '李四', '部门2', '4a3482e8-5205-4204-839e-4601e112e3ae', '1');
INSERT INTO `departments` VALUES ('4a3482e8-5205-4204-839e-4601e112e3a5', '10001001', '12', '1', NULL, '4a3482e8-5205-4204-839e-4601e112e3aa', 0, 'lisi', '部门3', '4a3482e8-5205-4204-839e-4601e112e3a1', '1');
INSERT INTO `departments` VALUES ('4a3482e8-5205-4204-839e-4601e112e3ae', '100', 'bumen1', '123123123', NULL, '00000000-0000-0000-0000-000000000000', 0, '张三', '部门1', '00000000-0000-0000-0000-000000000000', '12123');

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Icon` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ParentId` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SerialNumber` int(11) NOT NULL,
  `Type` int(11) NOT NULL,
  `Url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('08d5da34-db5c-e0e2-1768-987bf8c3a963', '101', 'User', '&#xe609;', '多级菜单示例', '00000000-0000-0000-0000-000000000000', NULL, 0, 0, NULL);
INSERT INTO `menus` VALUES ('08d5da65-3a84-5ec3-3184-ad7429cc824b', '101002', '233', '&#xe659;', '测试菜单11', '08d5da34-db5c-e0e2-1768-987bf8c3a963', NULL, 1, 0, '/Menu/index');
INSERT INTO `menus` VALUES ('08d5da6a-a5c7-c872-9779-53cb7a5a748d', '103', 'System', '&#xe614;', '系统管理', '00000000-0000-0000-0000-000000000000', NULL, 0, 0, NULL);
INSERT INTO `menus` VALUES ('08d5fc65-2226-a76a-894b-dd517a754d51', '10301', 'User', '&#xe613;', '用户管理', '08d5da6a-a5c7-c872-9779-53cb7a5a748d', NULL, 0, 0, '/User/Index');
INSERT INTO `menus` VALUES ('08d5fc66-495b-54f1-b38e-67369e3ff81f', '10302', 'Menu', '&#xe671;', '菜单管理', '08d5da6a-a5c7-c872-9779-53cb7a5a748d', NULL, 1, 0, '/Menu/Index');
INSERT INTO `menus` VALUES ('08d5fc68-5025-3804-1307-f86d52647eec', '1030201', 'loadTopMenu', '&#xe604;', '加载顶级菜单', '08d5fc66-495b-54f1-b38e-67369e3ff81f', NULL, 5, 1, '/Menu/loadTopMenu');
INSERT INTO `menus` VALUES ('08d5fc68-6b42-c3ef-78df-a8c161f06fc6', '1030202', 'addMenu', '&#xe654;', '新增菜单', '08d5fc66-495b-54f1-b38e-67369e3ff81f', NULL, 1, 1, '/Menu/Edit');
INSERT INTO `menus` VALUES ('08d5fc6d-b9de-9b86-9793-25f26508d437', '1030203', 'editMenu', '&#xe642;', '编辑', '08d5fc66-495b-54f1-b38e-67369e3ff81f', NULL, 2, 1, '/Menu/Edit');
INSERT INTO `menus` VALUES ('08d5fc6d-c807-9477-91ab-7c9c7d03707f', '1030204', 'delMenu', '&#xe640;', '删除', '08d5fc66-495b-54f1-b38e-67369e3ff81f', NULL, 3, 1, '/Menu/DeleteMuti');
INSERT INTO `menus` VALUES ('08d5fd25-1cc1-c567-5747-b61ad488879b', '1030101', 'addUser', '&#xe654;', '添加用户', '08d5fc65-2226-a76a-894b-dd517a754d51', NULL, 0, 1, '/User/Edit');
INSERT INTO `menus` VALUES ('08d5fd25-3083-f2b3-f273-5583a67317f3', '1030102', 'editUser', '&#xe642;', '编辑', '08d5fc65-2226-a76a-894b-dd517a754d51', NULL, 1, 1, '/User/Edit');
INSERT INTO `menus` VALUES ('08d5fd25-5029-81af-1af9-b3d34b86ae82', '1030103', 'disableUser', '&#xe69c;', '禁用', '08d5fc65-2226-a76a-894b-dd517a754d51', NULL, 2, 1, '/User/DisableMuti');
INSERT INTO `menus` VALUES ('0dc33899-520e-4dd2-9f70-80c4f16b2797', '1030302', 'addTopDepart', '&#xe654;', '新增顶级部门', 'f5c47a45-a2f2-4b06-8d9d-e13aa18a9240', NULL, 1, 1, '/Department/Edit');
INSERT INTO `menus` VALUES ('12176ed7-4c84-436e-87c8-6b2f02c2bc96', '1030304', 'delDepart', '&#xe640;', '删除', 'f5c47a45-a2f2-4b06-8d9d-e13aa18a9240', NULL, 3, 2, '/Department/DeleteMuti');
INSERT INTO `menus` VALUES ('1f39ff45-5caa-4fff-a0b8-268f7f28ddde', '1010020010101', '123', '&#xe659;', '123', '4a3b2a48-af80-4ab4-a69f-6a42c2bbd906', NULL, 0, 0, NULL);
INSERT INTO `menus` VALUES ('49f8a8a1-9d1b-49e6-b506-47b48d5d1ae1', '1030104', 'ableUser', '&#xe6af;', '启用', '08d5fc65-2226-a76a-894b-dd517a754d51', NULL, 0, 1, '/User/AbleMuti');
INSERT INTO `menus` VALUES ('4a3b2a48-af80-4ab4-a69f-6a42c2bbd906', '10100200101', '1', '&#xe6b2;', '菜单测试13', 'a2bfdd72-aded-4b36-9920-415c35ca7248', NULL, 0, 0, NULL);
INSERT INTO `menus` VALUES ('7b9bffbb-8dd1-4c93-a2d6-3850a88440f3', '1030303', 'editDepart', '&#xe642;', '编辑', 'f5c47a45-a2f2-4b06-8d9d-e13aa18a9240', NULL, 2, 2, '/Department/Edit');
INSERT INTO `menus` VALUES ('90fbb88e-56eb-4466-9eba-da586312be81', '1030404', 'authRole', '&#xe631;', '授权', 'ea4b7112-20ce-405b-a717-5d5dfec2659c', NULL, 4, 1, '/Role/SavePermission');
INSERT INTO `menus` VALUES ('a2bfdd72-aded-4b36-9920-415c35ca7248', '101002001', 'ceshi', '&#xe705;', '测试菜单11', '08d5da65-3a84-5ec3-3184-ad7429cc824b', NULL, 0, 0, 'admin/');
INSERT INTO `menus` VALUES ('b6341eed-b5c9-4651-b4e4-36a671361ed1', '1030301', 'addDepart', '&#xe654;', '新增', 'f5c47a45-a2f2-4b06-8d9d-e13aa18a9240', NULL, 1, 2, '/Department/Edit');
INSERT INTO `menus` VALUES ('b8f5571c-a362-4cb7-8d58-69ee014f280d', '1030401', 'addRole', '&#xe654;', '新增', 'ea4b7112-20ce-405b-a717-5d5dfec2659c', NULL, 1, 1, '/Role/Edit');
INSERT INTO `menus` VALUES ('c56ea1e2-3c7c-4d63-8340-e096317c9b26', '1030402', 'editRole', '&#xe642;', '编辑', 'ea4b7112-20ce-405b-a717-5d5dfec2659c', NULL, 2, 1, '/Role/Edit');
INSERT INTO `menus` VALUES ('df484a79-d1a0-4de8-86bb-5baaf277218a', '1030403', 'delRole', '&#xe640;', '删除', 'ea4b7112-20ce-405b-a717-5d5dfec2659c', NULL, 3, 1, '/Role/DeleteMuti');
INSERT INTO `menus` VALUES ('ea4b7112-20ce-405b-a717-5d5dfec2659c', '10304', 'Role', '&#xe705;', '角色配置', '08d5da6a-a5c7-c872-9779-53cb7a5a748d', NULL, 0, 0, '/Role/Index');
INSERT INTO `menus` VALUES ('f5c47a45-a2f2-4b06-8d9d-e13aa18a9240', '10303', 'Department', '&#xe630;', '部门管理', '08d5da6a-a5c7-c872-9779-53cb7a5a748d', NULL, 0, 0, '/Department/Index');

-- ----------------------------
-- Table structure for rolemenus
-- ----------------------------
DROP TABLE IF EXISTS `rolemenus`;
CREATE TABLE `rolemenus`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `RoleId` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MenuId` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of rolemenus
-- ----------------------------
INSERT INTO `rolemenus` VALUES ('012e6475-51e8-409f-8a58-cc42abb9cd23', '08d5da43-86e7-403c-1646-859b62d02ec3', 'a2bfdd72-aded-4b36-9920-415c35ca7248');
INSERT INTO `rolemenus` VALUES ('27cc363e-3b53-4213-9ad8-4b23c36df4d3', '08d5da43-86e7-403c-1646-859b62d02ec3', '08d5da65-3a84-5ec3-3184-ad7429cc824b');
INSERT INTO `rolemenus` VALUES ('2f4176f5-fe15-46c5-8694-1bdb28c6401c', '08d5da43-86e7-403c-1646-859b62d02ec3', '08d5fc65-2226-a76a-894b-dd517a754d51');
INSERT INTO `rolemenus` VALUES ('692beb53-fc0e-48e1-8ad7-69dc8dd6fcc8', '08d5da37-9d89-ba14-b88e-8ebb2df92ea8', '08d5da34-db5c-e0e2-1768-987bf8c3a963');
INSERT INTO `rolemenus` VALUES ('8c55bc5d-1be6-474d-a00d-5c80e36fcc5f', '08d5da37-9d89-ba14-b88e-8ebb2df92ea8', '08d5da65-3a84-5ec3-3184-ad7429cc824b');
INSERT INTO `rolemenus` VALUES ('c237ae3b-07fd-4206-bfb5-c3395a5c93fe', '08d5da43-86e7-403c-1646-859b62d02ec3', '08d5da6a-a5c7-c872-9779-53cb7a5a748d');
INSERT INTO `rolemenus` VALUES ('de941c13-23a3-447c-8301-3846ddf53004', '08d5da37-9d89-ba14-b88e-8ebb2df92ea8', 'a2bfdd72-aded-4b36-9920-415c35ca7248');
INSERT INTO `rolemenus` VALUES ('edba974e-21be-4f2c-91bd-1ed1d8c47145', '08d5da37-9d89-ba14-b88e-8ebb2df92ea8', '08d5fc65-2226-a76a-894b-dd517a754d51');
INSERT INTO `rolemenus` VALUES ('effea3ca-0f2e-45e5-9e24-6327cf775575', '08d5da37-9d89-ba14-b88e-8ebb2df92ea8', '08d5da6a-a5c7-c872-9779-53cb7a5a748d');
INSERT INTO `rolemenus` VALUES ('f002e02a-613b-4e7b-bfda-49cff244c557', '08d5da43-86e7-403c-1646-859b62d02ec3', '08d5da34-db5c-e0e2-1768-987bf8c3a963');
INSERT INTO `rolemenus` VALUES ('fd80c167-9ab1-49af-9781-6fd186a80736', '08d5da37-9d89-ba14-b88e-8ebb2df92ea8', '08d5fd25-1cc1-c567-5747-b61ad488879b');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `Id` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CreateTime` datetime(0) NULL DEFAULT NULL,
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Remarks` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('08d5da37-9d89-ba14-b88e-8ebb2df92ea8', '001', '2018-06-25 09:04:34', '00000000-0000-0000-0000-000000000000', '管理员', NULL);
INSERT INTO `roles` VALUES ('08d5da43-86e7-403c-1646-859b62d02ec3', '002', '2018-06-25 10:29:50', '00000000-0000-0000-0000-000000000000', '部门经理', '123456');
INSERT INTO `roles` VALUES ('c836407f-164a-403e-b485-79dc512997c8', 'addRole', '2018-09-10 14:59:41', '00000000-0000-0000-0000-000000000000', '1111es', '二手');

-- ----------------------------
-- Table structure for userroles
-- ----------------------------
DROP TABLE IF EXISTS `userroles`;
CREATE TABLE `userroles`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UserId` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RoleId` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of userroles
-- ----------------------------
INSERT INTO `userroles` VALUES ('011eead5-6920-4907-981b-f3aea001ac08', '87c4f6bc-b041-4d1d-b0b9-6bf415ac7027', '08d5da43-86e7-403c-1646-859b62d02ec3');
INSERT INTO `userroles` VALUES ('0c5157d7-affd-4d72-8005-3522a9142075', 'c3e12019-5ae7-4b9c-9dfa-7819e5467488', '08d5da43-86e7-403c-1646-859b62d02ec3');
INSERT INTO `userroles` VALUES ('38ebc287-1a1d-4c82-a6b9-5d78a459233f', '87c4f6bc-b041-4d1d-b0b9-6bf415ac7027', '08d5da37-9d89-ba14-b88e-8ebb2df92ea8');
INSERT INTO `userroles` VALUES ('42cea15d-53c0-4e08-b68b-ecab325bc4af', '00000000-0000-0000-0000-000000000000', '08d5da37-9d89-ba14-b88e-8ebb2df92ea8');
INSERT INTO `userroles` VALUES ('6b23a97d-b405-4d4d-91ce-038ea96f8a4e', 'a82c34e0-0031-46af-b3cb-c03499b3bb36', '08d5da37-9d89-ba14-b88e-8ebb2df92ea8');
INSERT INTO `userroles` VALUES ('ba235c3b-50eb-4393-a4b4-d91d72659107', '00000000-0000-0000-0000-000000000000', '08d5da43-86e7-403c-1646-859b62d02ec3');
INSERT INTO `userroles` VALUES ('ebfc940f-a261-4874-bf2f-bebd8096d92e', '683d854a-faf1-4a57-9224-8010cd716617', '08d5da43-86e7-403c-1646-859b62d02ec3');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UserName` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DepartmentId` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `EMail` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LoginTimes` int(11) NOT NULL,
  `MobileNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CreateUserId` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IsDeleted` int(11) NOT NULL,
  `CreateTime` datetime(0) NULL DEFAULT NULL,
  `LastLoginTime` datetime(0) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `IX_Users_DepartmentId`(`DepartmentId`) USING BTREE,
  CONSTRAINT `FK_Users_Departments_DepartmentId` FOREIGN KEY (`DepartmentId`) REFERENCES `departments` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('00000000-0000-0000-0000-000000000000', 'admin', '123123', '管理员', '4a3482e8-5205-4204-839e-4601e112e3ae', NULL, 398, NULL, NULL, '00000000-0000-0000-0000-000000000000', 0, '2018-06-26 16:04:59', '2018-09-10 15:09:17');
INSERT INTO `users` VALUES ('64f3c217-34d1-46ec-8948-3e578e2e13be', '123123', '123123', '12312', '4a3482e8-5205-4204-839e-4601e112e3a1', NULL, 0, NULL, NULL, '00000000-0000-0000-0000-000000000000', 0, '2018-08-18 20:54:11', '0001-01-01 00:00:00');
INSERT INTO `users` VALUES ('683d854a-faf1-4a57-9224-8010cd716617', '3214', '123123', '4123', '142415ba-c154-4f17-89e5-578b812c0cb4', '4123', 0, '124', NULL, '00000000-0000-0000-0000-000000000000', 0, '2018-09-10 09:54:43', '0001-01-01 00:00:00');
INSERT INTO `users` VALUES ('87c4f6bc-b041-4d1d-b0b9-6bf415ac7027', '1234556', NULL, '88888', '4a3482e8-5205-4204-839e-4601e112e3ae', NULL, 0, NULL, NULL, '00000000-0000-0000-0000-000000000000', 0, '2018-08-15 22:33:15', '0001-01-01 00:00:00');
INSERT INTO `users` VALUES ('a82c34e0-0031-46af-b3cb-c03499b3bb36', '123333', '123123', '333', '4a3482e8-5205-4204-839e-4601e112e3ae', '3', 0, '3', NULL, '00000000-0000-0000-0000-000000000000', 0, '2018-09-05 15:19:04', '0001-01-01 00:00:00');
INSERT INTO `users` VALUES ('b0f83055-f19c-4185-9ff1-904d3eaaa97a', '111', '123123', '1111', '4a3482e8-5205-4204-839e-4601e112e3ae', NULL, 0, NULL, NULL, '00000000-0000-0000-0000-000000000000', 0, '2018-08-18 17:33:51', '0001-01-01 00:00:00');

SET FOREIGN_KEY_CHECKS = 1;
