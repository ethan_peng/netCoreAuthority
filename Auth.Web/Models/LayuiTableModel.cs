﻿using System.Collections.Generic;

namespace Auth.Web.Models
{
    public class LayuiTableModel<T> where T : class
    {
        public bool Success { get; set; } = true;

        /// <summary>
        /// 成功为0
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        public string Msg { get; set; }

        /// <summary>
        /// 总条数
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public List<T> Data { get; set; }
    }
}