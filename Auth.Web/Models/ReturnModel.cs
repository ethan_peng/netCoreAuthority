﻿namespace Auth.Web.Models
{
    public class ReturnModel
    {
        public object Data { get; set; }
        public bool Success { get; set; } = true;
        public string Msg { get; set; } = "success";
        public int Code { get; set; } = 0;
    }
}