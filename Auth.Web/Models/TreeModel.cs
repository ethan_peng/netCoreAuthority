﻿namespace Auth.Web.Models
{
    public class TreeModel
    {
        public string Id { get; set; }

        public string Text { get; set; }

        public string Parent { get; set; }
        public string IconSkin { get; set; }
        public bool Checked { get; set; } = false;
    }
}
