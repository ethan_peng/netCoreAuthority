﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Web.Models
{
    public class SelectModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Selected { get; set; }
        public string Disabled { get; set; }
        /// <summary>
        /// 默认空 ，optgroup 为分组
        /// </summary>
        public string Type { get; set; }
    }

    public class SelectTree
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Selected { get; set; }
        public bool Disabled { get; set; }
        /// <summary>
        /// 默认空 ，optgroup 为分组
        /// </summary>
        public string Type { get; set; }
        public List<SelectTree> Children { get; set; }
    }
}