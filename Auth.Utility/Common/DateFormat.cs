﻿using Newtonsoft.Json.Converters;

namespace Auth.Utility.Common
{
    /// <summary>
    /// Newtonsoft 转换json 时时间格式
    /// </summary>
    public class DateFormat : IsoDateTimeConverter
    {
        public DateFormat()
        {
            DateTimeFormat = "yyyy-MM-dd";
        }
    }
    public class DateTimeFormat : IsoDateTimeConverter
    {
        public DateTimeFormat()
        {
            DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        }

    }
}