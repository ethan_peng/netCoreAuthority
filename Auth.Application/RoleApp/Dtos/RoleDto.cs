﻿using System;
using System.ComponentModel.DataAnnotations;
using Auth.SqlSugar.Attribute;
using Auth.Utility.Common;
using Newtonsoft.Json;

namespace Auth.Application.Dtos
{
    public class RoleDto
    {
        [DataTable(Type = "checkbox")]
        public string Id { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        [DataTable(Title = "编码", Search = "text")]
        [Required(ErrorMessage = "功能编码不能为空。")]
        public string Code { get; set; }
        /// <summary>
        /// 角色名
        /// </summary>
        [DataTable(Title = "角色名", Search = "text")]
        [Required(ErrorMessage = "角色名不能为空。")]
        public string Name { get; set; }

        public string CreateUserId { get; set; }

        [DataTable(Title = "添加时间", Search = "time")]
        [JsonConverter(typeof(DateTimeFormat))]
        public DateTime? CreateTime { get; set; }

        [DataTable(Title = "备注", Search = "text")]
        public string Remarks { get; set; }
    }
}