﻿using Auth.Application.Dtos;
using System;
using System.ComponentModel.DataAnnotations;

namespace Auth.Application.Dtos
{
    public class RoleMenuDto
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "未获取到角色。")]
        public string RoleId { get; set; }
        public RoleDto Role { get; set; }
        [Required(ErrorMessage = "未获取到菜单。")]
        public string MenuId { get; set; }
        public MenuDto Menu { get; set; }
    }
}