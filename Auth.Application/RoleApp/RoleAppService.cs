﻿using System;
using AutoMapper;
using Auth.Application.Dtos;
using System.Collections.Generic;
using System.Linq.Expressions;
using Auth.Domain.TableEntity;
using Auth.Repository.IRepositories;
using Auth.Utility;
using SqlSugar;

namespace Auth.Application
{
    public class RoleAppService : IRoleAppService
    {
        private readonly IRoleRepository _repository;
        public RoleAppService(IRoleRepository repository)
        {
            _repository = repository;
        }
        public int Delete(List<string> ids)
        {
            return _repository.DeleteWithCache(ids);
        }
        public int Insert(RoleDto entity)
        {
            return _repository.InsertWithCache(Mapper.Map<roles>(entity));
        }

        public int InsertRange(List<RoleDto> entities)
        {
            return _repository.InsertRangeWithCache(Mapper.Map<List<roles>>(entities));
        }

        public int Update(RoleDto entity)
        {
            return _repository.UpdateWithCache(Mapper.Map<roles>(entity));
        }

        /// <summary>
        /// 新增或修改
        /// </summary>
        /// <param name="dto">实体</param>
        /// <returns></returns>
        public bool InsertOrUpdate(RoleDto dto)
        {
            if (string.IsNullOrEmpty(dto.Id))
            {
                if (_repository.Any(s => s.Name == dto.Name || s.Code == dto.Code))
                    throw new MessageBox("角色名称或编号不能重复");
                dto.Id = Guid.NewGuid().ToString();
                return _repository.InsertWithCache(Mapper.Map<roles>(dto)) > 0;
            }

            return _repository.UpdateWithCache(Mapper.Map<roles>(dto)) > 0;
        }

        /// <summary>
        /// 根据角色获取权限
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllMenuListByRole(string roleId)
        {
            return _repository.GetAllMenuListByRole(roleId);
        }

        /// <summary>
        /// 更新角色权限关联关系
        /// </summary>
        /// <param name="roleId">角色id</param>
        /// <param name="roleMenus">角色权限集合</param>
        /// <returns></returns>
        public bool UpdateRoleMenu(string roleId, List<RoleMenuDto> roleMenus)
        {
            return _repository.UpdateRoleMenu(roleId, Mapper.Map<List<rolemenus>>(roleMenus));
        }

        public List<RoleDto> GetAll()
        {
            return Mapper.Map<List<RoleDto>>(_repository.QueryAll());
        }

        public LayuiTableModel<RoleDto> GetTableList(int page, int size, List<IConditionalModel> searchParam,
            string orderField="")
        {
            LayuiTableModel<RoleDto> tableModel = new LayuiTableModel<RoleDto>
            {
                Success = true,
                Code = 0,
                Count = _repository.QueryCount(searchParam),
                Data = Mapper.Map<List<RoleDto>>(_repository.QueryByWherePage(page, size, searchParam,
                    orderField))
            };
            return tableModel;
        }

        public RoleDto QuerySingle(string id)
        {
            return Mapper.Map<RoleDto>(_repository.QuerySingle(id));
        }

        public List<UserRoleDto> GetRoleByUser(string userId)
        {
            return Mapper.Map<List<UserRoleDto>>(_repository.GetRoleByUser(userId));
        }
    }
}