﻿using Auth.Application.Dtos;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Auth.Domain.TableEntity;
using Auth.Utility;
using SqlSugar;

namespace Auth.Application
{
    public interface IRoleAppService
    {
        /// <summary>
        /// 插入数据 返回受影响行数
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        int Insert(RoleDto entity);

        /// <summary>
        /// 批量插入数据
        /// </summary>
        /// <param name="entities">实体集合</param>
        /// <returns></returns>
        int InsertRange(List<RoleDto> entities);

        /// <summary>
        /// 根据主键批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        int Delete(List<string> ids);

        /// <summary>
        /// 更新(根据主键)
        /// </summary>
        /// <param name="model">实体对象</param>
        /// <returns></returns>
        int Update(RoleDto model);

        /// <summary>
        /// 新增或修改
        /// </summary>
        /// <param name="dto">实体</param>
        /// <returns></returns>
        bool InsertOrUpdate(RoleDto dto);

        /// <summary>
        /// 根据角色获取权限
        /// </summary>
        /// <param name="roleId">角色id</param>
        /// <returns></returns>
        List<string> GetAllMenuListByRole(string roleId);

        /// <summary>
        /// 更新角色权限关联关系
        /// </summary>
        /// <param name="roleId">角色id</param>
        /// <param name="roleMenus">角色权限集合</param>
        /// <returns></returns>
        bool UpdateRoleMenu(string roleId, List<RoleMenuDto> roleMenus);

        List<RoleDto> GetAll();
        /// <summary>
        /// 查询数据表格
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="whereExpression"></param>
        /// <param name="orderExpression"></param>
        /// <returns></returns>
        LayuiTableModel<RoleDto> GetTableList(int page, int size, List<IConditionalModel> searchParam,
            string orderField="");
        /// <summary>
        /// 获取单个实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        RoleDto QuerySingle(string id);

        /// <summary>
        /// 根据用户id获取角色
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<UserRoleDto> GetRoleByUser(string userId);
    }
}