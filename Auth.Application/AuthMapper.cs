﻿using Auth.Application.Dtos;
using Auth.Domain.TableEntity;
using AutoMapper;

namespace Auth.Application
{
    public class AuthMapper
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<menus, MenuDto>();
                cfg.CreateMap<MenuDto, menus>();
                cfg.CreateMap<departments, DepartmentDto>();
                cfg.CreateMap<DepartmentDto, departments>();
                cfg.CreateMap<RoleDto, roles>();
                cfg.CreateMap<roles, RoleDto>();
                cfg.CreateMap<RoleMenuDto, rolemenus>();
                cfg.CreateMap<rolemenus, RoleMenuDto>();
                cfg.CreateMap<UserDto, users>();
                cfg.CreateMap<users, UserDto>();
                cfg.CreateMap<UserRoleDto, userroles>();
                cfg.CreateMap<userroles, UserRoleDto>();
            });
        }
    }
}