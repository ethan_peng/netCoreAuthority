﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Auth.Application.Dtos;
using Auth.Domain.TableEntity;
using Auth.Utility;
using SqlSugar;

namespace Auth.Application
{
    public interface IUserAppService
    {
        users CheckUser(string userName, string password);

        /// <summary>
        /// 插入数据 返回受影响行数
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        int Insert(UserDto entity);

        /// <summary>
        /// 批量插入数据
        /// </summary>
        /// <param name="entities">实体集合</param>
        /// <returns></returns>
        int InsertRange(List<UserDto> entities);

        /// <summary>
        /// 根据主键批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        int Delete(List<string> ids);

        /// <summary>
        /// 更新(根据主键)
        /// </summary>
        /// <param name="model">实体对象</param>
        /// <returns></returns>
        int Update(UserDto model);

        bool InsertOrUpdate(UserDto dto);

        /// <summary>
        /// 查询数据表格
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="whereExpression"></param>
        /// <param name="orderExpression"></param>
        /// <returns></returns>
        LayuiTableModel<UserDto> GetTableList(int page, int size, List<IConditionalModel> searchParam,
            string orderField="");
        /// <summary>
        /// 获取单个实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        UserDto QuerySingle(string id);

        /// <summary>
        /// 禁用用户
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="isDisable">true禁用，false启用</param>
        /// <returns></returns>
        int DisableMuti(List<string> ids,bool isDisable);
        /// <summary>
        /// 更新用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        int UpdateUser(users user);
        /// <summary>
        /// 新增用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        int Insertuser(users user);
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        int ChangePwd(users user);
    }

}
