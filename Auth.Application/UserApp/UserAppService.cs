﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Auth.Application.Dtos;
using Auth.Domain.TableEntity;
using Auth.Repository.IRepositories;
using Auth.Utility;
using AutoMapper;
using SqlSugar;

namespace Auth.Application
{
    /// <summary>
    /// 用户管理服务
    /// </summary>
    public class UserAppService : IUserAppService
    {
        //用户管理仓储接口
        private readonly IUserRepository _repository;

        /// <summary>
        /// 构造函数 实现依赖注入
        /// </summary>
        /// <param name="userRepository">仓储对象</param>
        public UserAppService(IUserRepository userRepository)
        {
            _repository = userRepository;
        }

        public int Delete(List<string> ids)
        {
            return _repository.DeleteWithCache(ids);
        }
        public int Insert(UserDto entity)
        {
            return _repository.InsertWithCache(Mapper.Map<users>(entity));
        }

        public int InsertRange(List<UserDto> entities)
        {
            return _repository.InsertRangeWithCache(Mapper.Map<List<users>>(entities));
        }

        public int Update(UserDto entity)
        {
            return _repository.UpdateWithCache(Mapper.Map<users>(entity));
        }

        public users CheckUser(string userName, string password)
        {
            return _repository.CheckUser(userName, password);
        }

        /// <summary>
        /// 新增或修改
        /// </summary>
        /// <param name="dto">实体</param>
        /// <returns></returns>
        public bool InsertOrUpdate(UserDto dto)
        {
            if (string.IsNullOrEmpty(dto.Id))
            {
                dto.Id = Guid.NewGuid().ToString();
                return _repository.InsertWithCache(Mapper.Map<users>(dto)) > 0;
            }

            return _repository.UpdateWithCache(Mapper.Map<users>(dto)) > 0;
        }

        public LayuiTableModel<UserDto> GetTableList(int page, int size, List<IConditionalModel> searchParam,
            string orderField="")
        {
            LayuiTableModel<UserDto> tableModel = new LayuiTableModel<UserDto>
            {
                Success = true,
                Code = 0,
                Count = _repository.QueryCount(searchParam),
                Data = Mapper.Map<List<UserDto>>(_repository.QueryByWherePage(page, size, searchParam,
                    orderField))
            };
            return tableModel;
        }

        public UserDto QuerySingle(string id)
        {
            return Mapper.Map<UserDto>(_repository.QuerySingle(id));
        }

        public int DisableMuti(List<string> ids, bool isDisable)
        {
            return _repository.DisableMuti(ids, isDisable);
        }
        
        public int UpdateUser(users user)
        {
            var u = _repository.QueryByWhereWithCache(s=>s.UserName==user.UserName,s=>s.Id).First();
            if(u.Id!=user.Id)
                throw new MessageBox("已经存在相同用户名的用户！");
            return _repository.UpdateUser(user);
        }

        public int Insertuser(users user)
        {
            var existUser = _repository.Any(s => s.UserName == user.UserName);
            if (existUser)
                throw new MessageBox("已经存在相同用户名的用户！");
            return _repository.Insertuser(user);
        }

        public int ChangePwd(users user)
        {
            return _repository.ChangePwd(user);
        }
    }
}