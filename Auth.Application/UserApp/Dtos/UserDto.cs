﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Auth.SqlSugar.Attribute;
using Auth.Utility.Common;
using Newtonsoft.Json;

namespace Auth.Application.Dtos
{
    public class UserDto
    {
        [DataTable(Type = "checkbox")]
        public string Id { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        [DataTable(Title = "登录名",Search = "text")]
        [Required(ErrorMessage = "登录名不能为空。")]
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [JsonIgnore]
        public string Password { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        [DataTable(Title = "中文名", Search = "text")]
        [Required(ErrorMessage = "姓名不能为空。")]
        public string Name { get; set; }

        /// <summary>
        /// 邮箱地址
        /// </summary>
        [DataTable(Title = "邮箱", Search = "text")]
        public string EMail { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        [DataTable(Title = "手机号", Search = "text")]
        public string MobileNumber { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUserId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DataTable(Title = "添加时间", Width="20%", Search = "time")]
        [JsonConverter(typeof(DateTimeFormat))]
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 上次登录时间
        /// </summary>
        public DateTime LastLoginTime { get; set; }

        /// <summary>
        /// 登录次数
        /// </summary>
        public int LoginTimes { get; set; }

        /// <summary>
        /// 部门ID
        /// </summary>
        [Required(ErrorMessage = "未获取到部门。")]
        public string DepartmentId { get; set; }

        /// <summary>
        /// 是否已删除
        /// </summary>
        [DataTable(Title = "状态", Templet="<div>{{#if(d.isDeleted===0){}}<span class=\"layui-badge layui-bg-green\">正常</span>{{#}else{}}<span class=\"layui-badge\">禁用</span>{{#}}}</div>", Search = "selector",SearchData = "[{value:0,name:'正常'},{value:1,name:'禁用'}]")]
        public int IsDeleted { get; set; }

        /// <summary>
        /// 角色集合
        /// </summary>
        public List<UserRoleDto> UserRoles { get; set; }

    }
}
