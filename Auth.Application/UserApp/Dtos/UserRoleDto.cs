﻿using System;

namespace Auth.Application.Dtos
{
    public class UserRoleDto
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string RoleId { get; set; }
    }
}