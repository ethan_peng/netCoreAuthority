﻿using Auth.Application.Dtos;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Auth.Domain.TableEntity;
using Auth.Utility;
using SqlSugar;

namespace Auth.Application
{
    public interface IDepartmentAppService
    {
        /// <summary>
        /// 插入数据 返回受影响行数
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        int Insert(DepartmentDto entity);

        /// <summary>
        /// 批量插入数据
        /// </summary>
        /// <param name="entities">实体集合</param>
        /// <returns></returns>
        int InsertRange(List<DepartmentDto> entities);

        /// <summary>
        /// 根据主键批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        int Delete(List<string> ids);
        /// <summary>
        /// 根据主键删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(string id);

        /// <summary>
        /// 更新(根据主键)
        /// </summary>
        /// <param name="model">实体对象</param>
        /// <returns></returns>
        int Update(DepartmentDto model);
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        List<DepartmentDto> GetAllList();

        /// <summary>
        /// 新增或修改
        /// </summary>
        /// <param name="dto">实体</param>
        /// <returns></returns>
        bool InsertOrUpdate(DepartmentDto dto);

        /// <summary>
        /// 查询数据表格
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="whereExpression"></param>
        /// <param name="searchParam"></param>
        /// <param name="orderExpression"></param>
        /// <returns></returns>
        LayuiTableModel<DepartmentDto> GetTableList(int page, int size, List<IConditionalModel> searchParam,
           string orderField="");
        /// <summary>
        /// 获取单个实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DepartmentDto QuerySingle(string id);

        /// <summary>
        /// 查询全部
        /// </summary>
        /// <returns></returns>
        List<DepartmentDto> QueryAll(Expression<Func<departments, bool>> whereExpression);

        /// <summary>
        /// 生成下级菜单No
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        string GetNextNo(string menuId);
        /// <summary>
        /// 获取同级菜单No
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        string GetNo(string menuId);
    }
}