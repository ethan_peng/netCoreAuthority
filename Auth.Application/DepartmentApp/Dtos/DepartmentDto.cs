﻿using System;
using Auth.SqlSugar.Attribute;

namespace Auth.Application.Dtos
{
    public class DepartmentDto
    {
        public string Id { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        [DataTable(Title = "部门名称")]
        public string Name { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        [DataTable(Title = "编号")]
        public string No { get; set; }

        /// <summary>
        /// 部门编号
        /// </summary>
        [DataTable(Title = "部门编码")]
        public string Code { get; set; }

        /// <summary>
        /// 部门负责人
        /// </summary>
        public string Manager { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string ContactNumber { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [DataTable(Title="备注")]
        public string Remarks { get; set; }

        /// <summary>
        /// 父级部门ID
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUserId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 是否已删除
        /// </summary>
        public int IsDeleted { get; set; }

        [DataTable(Title = "操作", Toolbar = "#toolButton",MinWidth =250)]
        public string ToolButton { get; set; }
    }
}