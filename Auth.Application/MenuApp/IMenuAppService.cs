﻿using System;
using Auth.Application.Dtos;
using System.Collections.Generic;
using System.Linq.Expressions;
using Auth.Domain.TableEntity;
using Auth.Utility;
using SqlSugar;

namespace Auth.Application
{
    public interface IMenuAppService
    {
        /// <summary>
        /// 插入数据 返回受影响行数
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        int Insert(MenuDto entity);

        /// <summary>
        /// 批量插入数据
        /// </summary>
        /// <param name="entities">实体集合</param>
        /// <returns></returns>
        int InsertRange(List<MenuDto> entities);

        /// <summary>
        /// 根据主键批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        int Delete(List<string> ids);

        /// <summary>
        /// 更新(根据主键)
        /// </summary>
        /// <param name="model">实体对象</param>
        /// <returns></returns>
        int Update(MenuDto model);
        /// <summary>
        /// 获取功能列表
        /// </summary>
        /// <returns></returns>
        List<MenuDto> GetAllList();

        /// <summary>
        /// 新增或修改功能
        /// </summary>
        /// <param name="dto">实体</param>
        /// <returns></returns>
        bool InsertOrUpdate(MenuDto dto);

        /// <summary>
        /// 根据用户获取功能菜单
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        List<MenuDto> GetMenusByUser(string userId);

        /// <summary>
        /// 生成下级菜单No
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        string GetNextNo(string menuId);
        /// <summary>
        /// 获取同级菜单No
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        string GetNo(string menuId);

        /// <summary>
        /// 查询数据表格
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="searchParam"></param>
        /// <param name="orderExpression"></param>
        /// <returns></returns>
        LayuiTableModel<MenuDto> GetTableList(int page, int size, List<IConditionalModel> searchParam,
            string orderFields);
        /// <summary>
        /// 获取单个实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        MenuDto QuerySingle(string id);
    }
}