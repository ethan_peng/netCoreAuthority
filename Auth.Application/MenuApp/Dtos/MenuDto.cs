﻿using System.ComponentModel.DataAnnotations;
using Auth.SqlSugar.Attribute;

namespace Auth.Application.Dtos
{
    public class MenuDto
    {
        [DataTable(Type = "checkbox")]
        public string Id { get; set; }

        /// <summary>
        /// 父级ID
        /// </summary>
        public string ParentId { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        [DataTable(Title = "编号",Search ="text")]
        [Required(ErrorMessage = "菜单编号不能为空。")]
        public string No { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [DataTable(Title = "排序")]
        public int SerialNumber { get; set; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        [Required(ErrorMessage = "菜单名称不能为空。")]
        [DataTable(Title = "菜单名",Search ="text")]
        public string Name { get; set; }

        /// <summary>
        /// 菜单编码
        /// </summary>
        [DataTable(Title = "编码",Search ="text")]
        [Required(ErrorMessage = "菜单编码不能为空。")]
        public string Code { get; set; }

        /// <summary>
        /// 菜单地址
        /// </summary>
        [DataTable(Title = "URL",Search ="text")]
        public string Url { get; set; }

        /// <summary>
        /// 类型0:菜单，1：表格头部按钮，2：表格内操作栏按钮
        /// </summary>
        [DataTable(Title = "类型",Templet = "<div>{{#if(d.type === 1){}}表头按钮{{#}else if(d.type===0){}}菜单{{#}else{}}操作栏按钮{{#}}}</div>")]
        public int Type { get; set; }

        /// <summary>
        /// 菜单图标
        /// </summary>
        [DataTable(Title = "图标",Templet = "<div><i class=\"layui-icon\">{{d.icon}}</i></div>")]
        public string Icon { get; set; }

        /// <summary>
        /// 菜单备注
        /// </summary>
        [DataTable(Title = "备注", Search = "text")]
        public string Remarks { get; set; }
        
    }
}