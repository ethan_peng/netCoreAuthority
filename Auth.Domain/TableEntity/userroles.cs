﻿using System;

namespace Auth.Domain.TableEntity
{
    ///<summary>
    ///
    ///</summary>
    public partial class userroles
    {
           public userroles(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RoleId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UserId {get;set;}

    }
}
