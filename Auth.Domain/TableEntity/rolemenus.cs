﻿
using SqlSugar;

namespace Auth.Domain.TableEntity
{
    ///<summary>
    ///
    ///</summary>
    public partial class rolemenus
    {
        public rolemenus()
        {


        }

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Id { get; set; }

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string MenuId { get; set; }

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string RoleId { get; set; }

        [SugarColumn(IsIgnore = true)]
        public roles Role { get; set; }

        [SugarColumn(IsIgnore = true)]
        public menus Menu { get; set; }

    }
}