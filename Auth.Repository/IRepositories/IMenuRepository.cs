﻿using Auth.Domain.TableEntity;
using Auth.SqlSugar.IRepositories;

namespace Auth.Repository.IRepositories
{
    public interface IMenuRepository :IBaseRepository <menus>
    {
    }
}
