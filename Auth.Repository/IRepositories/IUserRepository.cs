﻿using System;
using System.Collections.Generic;
using Auth.Domain.TableEntity;
using Auth.SqlSugar.IRepositories;

namespace Auth.Repository.IRepositories
{
    /// <summary>
    /// 用户管理仓储接口
    /// </summary>
    public interface IUserRepository : IBaseRepository<users>
    {
        /// <summary>
        /// 检查用户是存在
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <returns>存在返回用户实体，否则返回NULL</returns>
        users CheckUser(string userName, string password);

        users GetWithRoles(string id);
        int DisableMuti(List<string> ids, bool isDisable);
        /// <summary>
        /// 更新用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        int UpdateUser(users user);
        /// <summary>
        /// 新增用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        int Insertuser(users user);
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        int ChangePwd(users user);
    }
}
