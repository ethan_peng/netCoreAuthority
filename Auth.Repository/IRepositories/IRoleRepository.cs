﻿using System.Collections.Generic;
using Auth.Domain.TableEntity;
using Auth.SqlSugar.IRepositories;

namespace Auth.Repository.IRepositories
{
    public interface IRoleRepository : IBaseRepository<roles>
    {
        /// <summary>
        /// 根据角色获取权限
        /// </summary>
        /// <param name="roleId">角色id</param>
        /// <returns></returns>
        List<string> GetAllMenuListByRole(string roleId);

        /// <summary>
        /// 更新角色权限关联关系
        /// </summary>
        /// <param name="roleId">角色id</param>
        /// <param name="roleMenus">角色权限集合</param>
        /// <returns></returns>
        bool UpdateRoleMenu(string roleId, List<rolemenus> roleMenus);

        /// <summary>
        /// 根据用户id获取角色
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<userroles> GetRoleByUser(string userId);
    }
}
