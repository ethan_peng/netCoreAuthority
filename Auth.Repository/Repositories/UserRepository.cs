﻿using System;
using System.Collections.Generic;
using System.Linq;
using Auth.Domain.TableEntity;
using Auth.Repository.IRepositories;
using Auth.SqlSugar;
using Auth.SqlSugar.IocManager;
using Auth.SqlSugar.Repositories;
using Auth.Utility;
using Microsoft.Extensions.Configuration;
using SqlSugar;

namespace Auth.Repository.Repositories
{
    /// <summary>
    /// 用户管理仓储实现
    /// </summary>
    public class UserRepository : BaseRepository<users>, IUserRepository
    {
        public UserRepository()
        {
        }

        public users CheckUser(string userName, string password)
        {
            using (SqlSugarClient dbClient = DbInstance.GetInstance())
            {

                return dbClient.Queryable<users>().Where(it => it.UserName == userName && it.Password == password)
                    .First();

            }
        }

        public users GetWithRoles(string id)
        {
            using (SqlSugarClient dbClient = DbInstance.GetInstance())
            {
                var user = dbClient.Queryable<users>().InSingle(id);
                if (user != null)
                    user.UserRoles = dbClient.Queryable<userroles>().Where(s => s.UserId == id).WithCacheIF(RedisCache.UseCache).ToList();
                return user;
            }
        }

        public int DisableMuti(List<string> ids, bool isDisable)
        {
            using (SqlSugarClient dbClient = DbInstance.GetInstance())
            {
                if (isDisable)
                {
                    if (RedisCache.UseCache)
                        return dbClient.Updateable<users>()
                            .UpdateColumns(it => new users() { IsDeleted = 1 }).Where(s => ids.Contains(s.Id)).RemoveDataCache()
                            .ExecuteCommand();
                    else
                        return dbClient.Updateable<users>()
                               .UpdateColumns(it => new users() { IsDeleted = 1 }).Where(s => ids.Contains(s.Id))
                               .ExecuteCommand();

                }
                if (RedisCache.UseCache)
                    return dbClient.Updateable<users>()
                    .UpdateColumns(it => new users() { IsDeleted = 0 }).Where(s => ids.Contains(s.Id)).RemoveDataCache()
                    .ExecuteCommand();
                else
                    return dbClient.Updateable<users>()
                   .UpdateColumns(it => new users() { IsDeleted = 0 }).Where(s => ids.Contains(s.Id))
                   .ExecuteCommand();
            }
        }

        public int UpdateUser(users user)
        {
            using (SqlSugarClient dbClient = DbInstance.GetInstance())
            {
                try
                {
                    dbClient.Ado.BeginTran();
                    if (RedisCache.UseCache)
                    {
                        dbClient.Updateable(user).IgnoreColumns(s => new { s.Password }).RemoveDataCache().ExecuteCommand();
                        dbClient.Deleteable<userroles>().Where(s => s.UserId == user.Id).RemoveDataCache()
                            .ExecuteCommand();
                        return dbClient.Insertable(user.UserRoles).RemoveDataCache().ExecuteCommand();
                    }
                    else
                    {
                        dbClient.Updateable(user).IgnoreColumns(s => new { s.Password }).ExecuteCommand();
                        dbClient.Deleteable<userroles>().Where(s => s.UserId == user.Id)
                            .ExecuteCommand();
                        return dbClient.Insertable(user.UserRoles).ExecuteCommand();
                    }
                }
                catch (Exception ex)
                {
                    dbClient.Ado.RollbackTran();
                    throw new Exception(ex.Message);
                }
            }
        }

        public int Insertuser(users user)
        {
            using (SqlSugarClient dbClient = DbInstance.GetInstance())
            {
                try
                {
                    dbClient.Ado.BeginTran();
                    dbClient.Insertable(user).ExecuteCommand();
                    return dbClient.Insertable(user.UserRoles).ExecuteCommand();
                }
                catch (Exception ex)
                {
                    dbClient.Ado.RollbackTran();
                    throw new Exception(ex.Message);
                }
            }
        }

        public int ChangePwd(users user)
        {
            using (SqlSugarClient dbClient = DbInstance.GetInstance())
            {
                return dbClient.Updateable(user)
                    .UpdateColumns(it => new {it.Password})
                    .ExecuteCommand();
            }
        }
    }
}