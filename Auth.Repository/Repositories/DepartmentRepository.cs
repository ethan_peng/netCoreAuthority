﻿using Auth.Domain.TableEntity;
using Auth.Repository.IRepositories;
using Auth.SqlSugar.Repositories;

namespace Auth.Repository.Repositories
{
    public class DepartmentRepository : BaseRepository<departments>, IDepartmentRepository
    {

    }
}