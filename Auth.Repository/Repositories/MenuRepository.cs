﻿using Auth.Domain.TableEntity;
using Auth.Repository.IRepositories;
using Auth.SqlSugar;
using Auth.SqlSugar.Repositories;
using Auth.Utility;
using SqlSugar;
using System;
using System.Collections.Generic;

namespace Auth.Repository.Repositories
{
    public class MenuRepository : BaseRepository<menus>, IMenuRepository
    {
        public MenuRepository()
        {
           
        }
        public new int Delete(List<string> ids) {
            using (SqlSugarClient dbClient = DbInstance.GetInstance())
            {
                try
                {
                    dbClient.Ado.BeginTran();
                    if (RedisCache.UseCache)
                    {
                        dbClient.Deleteable<rolemenus>().Where(it => ids.Contains(it.MenuId)).RemoveDataCache().ExecuteCommand();
                        return dbClient.Deleteable<menus>().In(ids).RemoveDataCache().ExecuteCommand();
                    }
                    else
                    {
                        dbClient.Deleteable<rolemenus>().Where(it => ids.Contains(it.MenuId)).ExecuteCommand();
                        return dbClient.Deleteable<menus>().In(ids).ExecuteCommand();
                    }
                    
                }
                catch (Exception ex)
                {
                    dbClient.Ado.RollbackTran();
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}