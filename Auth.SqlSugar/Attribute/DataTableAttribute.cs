﻿using System;

namespace Auth.SqlSugar.Attribute
{
    /// <summary>
    /// 生成Layui数据表格表头
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class, Inherited = true)]
    public class DataTableAttribute : System.Attribute
    {
        /// <summary>
        /// 实体实际对应的表名
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// 字段名(不要填写自动获取)
        /// </summary>
        public string Field { get; set; }
        /// <summary>
        /// 中文列名
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 生成列名时候是否忽略该字段(不加属性会自动忽略)
        /// </summary>
        public bool Ignore { get; set; }

        /// 设定列宽（默认自动分配）。支持填写：数字、百分比。
        /// </summary>
        public string Width { get; set; }

        /// <summary>
        /// 局部定义当前常规单元格的最小宽度（默认：60）
        /// </summary>
        public int MinWidth { get; set; } = 60;

        /// <summary>
        /// 设定列类型。可选值有：normal（常规列，默认）、checkbox（复选框列）、space（空列）、numbers（序号列)radio(单选框)。
        /// </summary>
        public string Type { get; set; } = "normal";

        /// <summary>
        /// 是否全选状态（默认：false），必须复选框列开启后才有效。
        /// </summary>
        public bool LAY_CHECKED { get; set; } = false;
        /// <summary>
        /// 固定列。可选值有：left（固定在左）、right（固定在右）
        /// </summary>
        public string Fixed { get; set; }
        /// <summary>
        /// 是否初始隐藏列，默认：false。
        /// </summary>
        public bool Hide { get; set; } = false;
        /// <summary>
        /// 用于显示自定义的合计文本。
        /// </summary>
        public string TotalRowText { get; set; }
        /// <summary>
        /// 是否允许排序（默认：false）。如果设置 true，则在对应的表头显示排序icon，从而对列开启排序功能。
        /// </summary>
        public bool Sort { get; set; } = true;
        /// <summary>
        /// 是否禁用拖拽列宽（默认：false）。
        /// </summary>
        public bool Unresize { get; set; } = true;
        /// <summary>
        /// 单元格编辑类型（默认不开启）目前只支持：text（输入框）
        /// </summary>
        public string Edit { get; set; }
        /// <summary>
        /// 自定义单元格点击事件名，以便在 tool 事件中完成对该单元格的业务处理
        /// </summary>
        public string Event { get; set; }
        /// <summary>
        /// 自定义单元格样式。即传入 CSS 样式
        /// </summary>
        public string Style { get; set; }
        /// <summary>
        /// 单元格排列方式。可选值有：left（默认）、center（居中）、right（居右）
        /// </summary>
        public string Align { get; set; } = "left";
        /// <summary>
        /// 单元格所占列数（默认：1）。一般用于多级表头
        /// </summary>
        public int Colspan { get; set; } = 1;

        /// <summary>
        /// 单元格所占行数（默认：1）。一般用于多级表头
        /// </summary>
        public int Rowspan { get; set; } = 1;
        /// <summary>
        /// 自定义列模板，模板遵循 laytpl 语法。
        /// </summary>
        public string Templet { get; set; }
        /// <summary>
        /// 绑定列工具条。
        /// </summary>
        public string Toolbar { get; set; }


        #region 搜索
        /// <summary>
        /// 搜索字段格式 text，number，selector,time
        /// </summary>
        public string Search { get; set; }
        /// <summary>
        /// 下拉数据 "[{value:1,name:'合格'},{value:2,name:'不合格'}]" 支持url get请求 "{url:'/user/get'}"
        /// url返回json格式
        /// </summary>
        public string SearchData { get; set; }

        #endregion
    }
}