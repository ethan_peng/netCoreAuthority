﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Auth.SqlSugar.IRepositories
{
    public interface IBaseRepository<T> where T : class,new()
    {
        /// <summary>
        /// 插入数据 返回受影响行数
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
         int Insert(T entity);
        /// <summary>
        /// 插入数据，删除缓存
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        int InsertWithCache(T entity);

        /// <summary>
        /// 批量插入数据
        /// </summary>
        /// <param name="entities">实体集合</param>
        /// <returns></returns>
         int InsertRange(List<T> entities);
        /// <summary>
        /// 批量插入数据，删除缓存
        /// </summary>
        /// <param name="rntities"></param>
        /// <returns></returns>
        int InsertRangeWithCache(List<T> entities);
        /// <summary>
        /// 根据主键删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(string id);

        /// <summary>
        /// 根据主键删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int DeleteWithCache(string id);
        /// <summary>
        /// 根据主键批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        int Delete(List<string> ids);
        /// <summary>
        /// 根据主键批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        int DeleteWithCache(List<string> ids);
        /// <summary>
        /// 根据实体删除
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        int Delete(T t);
        /// <summary>
        /// 根据实体集合删除
        /// </summary>
        /// <param name="tList"></param>
        /// <returns></returns>
        int Delete(List<T> tList);
        /// <summary>
        /// 根据实体删除
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        int DeleteWithCache(T t);
        /// <summary>
        /// 根据实体集合删除
        /// </summary>
        /// <param name="tList"></param>
        /// <returns></returns>
        int DeleteWithCache(List<T> tList);

        /// <summary>
        /// 更新(根据主键)
        /// </summary>
        /// <param name="model">实体对象</param>
        /// <returns></returns>
        int Update(T model);

        /// <summary>
        /// 更新(根据主键),删除缓存
        /// </summary>
        /// <param name="model">实体对象</param>
        /// <returns></returns>
        int UpdateWithCache(T model);

        /// <summary>
        /// 更新(根据主键)
        /// </summary>
        /// <param name="model">实体对象</param>
        /// <param name="whereExpression">表达式</param>
        /// <returns></returns>
        int Update(T model, Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 批量更新(根据主键)
        /// </summary>
        /// <param name="model">entity</param>
        /// <returns></returns>
         int Update(List<T> model);

        /// <summary>
        /// 查询一个
        /// </summary>
        /// <returns></returns>
         T QuerySingle();

        /// <summary>
        /// 按条件查询一个
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
         T QuerySingle(string id);

        /// <summary>
        /// 查询所有
        /// </summary>
        /// <returns></returns>
         List<T> QueryAll();

        /// <summary>
        /// 查询全部，启用缓存
        /// </summary>
        /// <returns></returns>
        List<T> QueryAllWithCache();

        /// <summary>
        /// 按条件查询
        /// </summary>
        /// <param name="whereExpression">条件表达式</param>
        /// <param name="orderExpression">排序表达式</param>
        /// <param name="orderType">排序方式</param>
        /// <returns></returns>
         List<T> QueryByWhere(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, object>> orderExpression, OrderByType orderType = OrderByType.Asc);

        /// <summary>
        /// 按条件查询,缓存
        /// </summary>
        /// <param name="whereExpression">条件表达式</param>
        /// <param name="orderExpression">排序表达式</param>
        /// <param name="orderType">排序方式</param>
        /// <returns></returns>
        List<T> QueryByWhereWithCache(Expression<Func<T, bool>> whereExpression,
           Expression<Func<T, object>> orderExpression, OrderByType orderType = OrderByType.Asc);


        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">页容量</param>
        /// <param name="searchParam"></param>
        /// <param name="orderExpression"></param>
        /// <param name="orderType"></param>
        /// <returns></returns>
        List<T> QueryByWherePage(int pageIndex, int pageSize, List<IConditionalModel> searchParam,
            string orderFields="");

        /// <summary>
        /// 查询指定个数的数据
        /// </summary>
        /// <param name="takeNum">指定个数</param>
        /// <param name="whereExpression">条件表达式</param>
        /// <param name="orderExpression">排序表达式</param>
        /// <param name="orderType">排序方式</param>
        /// <returns></returns>
         List<T> QueryTop(int takeNum, Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, object>> orderExpression, OrderByType orderType = OrderByType.Asc);

        /// <summary>
        /// 查询条数
        /// </summary>
        /// <param name="whereExpression">条件表达式</param>
        /// <returns></returns>
         int QueryCount(Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 查询条数
        /// </summary>
        /// <returns></returns>
        int QueryCount(List<IConditionalModel> searchParam);
        /// <summary>
        /// 查询是否存在记录
        /// </summary>
        /// <param name="whereExpression">条件表达式</param>
        /// <returns></returns>
        bool Any(Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 执行sql语句或存储过程
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="sql">sql语句</param>
        /// <param name="whereObj">命令参数对应匿名对象</param>
        /// <returns></returns>
         List<TResult> QueryBySql<TResult>(string sql, object whereObj = null);

        /// <summary>
        /// 事务执行sql语句或存储过程
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="sql">sql语句</param>
        /// <param name="whereObj">命令参数对应匿名对象</param>
        /// <returns></returns>
        List<TResult> QueryBySqlTransactions<TResult>(string sql, object whereObj = null);

    }
}