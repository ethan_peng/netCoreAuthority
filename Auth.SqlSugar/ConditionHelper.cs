﻿using System.Collections.Generic;
using Newtonsoft.Json;
using SqlSugar;

namespace Auth.SqlSugar
{
    public static class ConditionHelper
    {
        public static List<IConditionalModel> GetCondition(string searchParam)
        {
            List<IConditionalModel> conModels = new List<IConditionalModel>();
            if (!string.IsNullOrEmpty(searchParam))
            {
                var searchParamList = JsonConvert.DeserializeObject<List<SearchParam>>(searchParam);
                foreach (var item in searchParamList)
                {
                    if (item.Conditional.ToLower() == "between")
                    {
                        conModels.Add(new ConditionalModel()
                        {
                            FieldName = item.Field,
                            ConditionalType = ConditionalType.GreaterThan,
                            FieldValue = item.Value.Split(",")[0]
                        });
                        conModels.Add(new ConditionalModel()
                        {
                            FieldName = item.Field,
                            ConditionalType = ConditionalType.LessThan,
                            FieldValue = item.Value.Split(",")[1]
                        });
                    }
                    else
                    {
                        conModels.Add(new ConditionalModel()
                        {
                            FieldName = item.Field,
                            ConditionalType = GetConditionType(item.Conditional),
                            FieldValue = item.Value
                        });
                    }

                }
            }

            return conModels;
        }

        public static ConditionalType GetConditionType(string conditionalType)
        {
            switch (conditionalType)
            {
                case "Equal":
                    return ConditionalType.Equal;
                case "GreaterThan":
                    return ConditionalType.GreaterThan;
                case "LessThan":
                    return ConditionalType.LessThan;
                case "Like":
                    return ConditionalType.Like;
                default:
                    return ConditionalType.Equal;
            }
        }
    }
    public class SearchParam
    {
        public string Conditional { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
    }
}
