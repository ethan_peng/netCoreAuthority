# netCoreAuthority

#### 项目介绍
基于 dotnetcore 2.*  的 rbac 权限管理基础架子。
coreSDK 版本 ````2.1.302```` [下载页面](https://www.microsoft.com/net/download/dotnet-core/2.1)<br/>
前端 layui，后端dotnetcore 2.1 Mysql 、sqlsugar（支持主流数据库 MySQL，SqlServer等）
#### 截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/0906/170413_510facef_740136.png "登录.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0906/170431_907e33e2_740136.png "动态查询.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0906/170442_ce65de84_740136.png "角色授权.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0906/170448_af9cf9ad_740136.png "树+列表.png")
#### TODO
- ~~权限控制到按钮~~
- ~~动态生成查询条件~~
- ~~打印~~（layui自带的，后期可能会做成后端支持）
- ~~Exce导出~~（（layui自带的，后期可能会做成后端支持）
- ~~动态生成数据表头~~
#### Linux 下验证码显示问题 ([来源](https://www.cnblogs.com/yuangang/p/6000460.html))
>  yum install autoconf automake libtool

>  yum install freetype-devel fontconfig libXft-devel

>  yum install libjpeg-turbo-devel libpng-devel giflib-devel libtiff-devel libexif-devel

> yum install glib2-devel cairo-devel

> git clone https://github.com/mono/libgdiplus (如果没有安装git命令 执行yum install git 安装)

> wget http://dl.fedoraproject.org/pub/epel/7/x86_64/Packages/l/libgdiplus-2.10-10.el7.x86_64.rpm （如果没有安装wget命令 执行yum install wget,如果404 去http://dl.fedoraproject.org/pub/epel/7/x86_64/Packages/l/看下最新的版本号）

>  wget http://dl.fedoraproject.org/pub/epel/7/x86_64/Packages/l/libgdiplus-devel-2.10-10.el7.x86_64.rpm 

> rpm -Uvh libgdiplus-2.10-9.el7.x86_64.rpm

> rpm -Uvh libgdiplus-devel-2.10-9.el7.x86_64.rpm

> cd libgdiplus

>  ./autogen.sh

> make

> make install

> cd /usr/lib64/

>ln -s /usr/local/lib/libgdiplus.so gdiplus.dll

> 在windows 系统下将"Verdana", "Microsoft Sans Serif", "Comic Sans MS", "Arial", "宋体" 复制到/usr/share/fonts/chinese/TrueType 目录下（chinese/TrueType 两个目录是自己创建的）

> cd /usr/share/fonts/chinese/TrueType

> mkfontscale（如果提示 mkfontscale: command not found，需自行安装 # yum install mkfontscale ）

> mkfontdir

> fc-cache -fv（如果提示 fc-cache: command not found，则需要安装# yum install fontconfig ）

